import json
import os
import subprocess

# Call pre-setup.py
subprocess.run(["python3", ".devcontainer/pre_setup.py"])


def confirm_overwrite(file_path):
    """Prompt the user for confirmation to overwrite a file if it exists."""
    if os.path.exists(file_path):
        while True:
            choice = input(
                f"{file_path} already exists. Do you want to overwrite it? (yes/no): ").strip().lower()
            if choice in ['yes', 'no']:
                return choice == 'yes'
    return True


# Template JSON data
template = {
    "extends": "",
    "context": "../",
    "dockerFile": "Dockerfile",
    "runArgs": [
        "--cap-add=NET_ADMIN",
        "--security-opt",
        "seccomp=unconfined",
        "--network=host"
    ],
    "remoteUser": "root",
    "containerUser": "root",
    "postStartCommand": "git config --global --add safe.directory '*'",
    "mounts": [
        "source=${localWorkspaceFolderBasename}-build,target=${containerWorkspaceFolder}/build,type=volume",
        "source=${localWorkspaceFolderBasename}-install,target=${containerWorkspaceFolder}/install,type=volume",
        "source=${localWorkspaceFolderBasename}-log,target=${containerWorkspaceFolder}/log,type=volume",
        "source=/tmp/mcap,target=/tmp/mcap,type=bind",
        "source=${localEnv:SSH_AUTH_SOCK},target=/ssh-agent,type=bind,consistency=cached"
    ],
    "containerEnv": {
        "SSH_AUTH_SOCK": "/ssh-agent",
        "DISPLAY": "${localEnv:DISPLAY}",
        "ROS_DOMAIN_ID": "127",
        "RMW_IMPLEMENTATION": "rmw_cyclonedds_cpp",
        "CYCLONEDDS_URI": "${containerWorkspaceFolder}/config/dds_config.xml"
    },
    "customizations": {
        "vscode": {
            "settings": {
                "extensions.verifySignature": False,
            },
            "extensions": [
                "ms-azuretools.vscode-docker",
                "eamodio.gitlens",
                "ms-python.python",
                "ms-vscode.cpptools",
                "ms-python.autopep8",
                "ms-vscode.cpptools-extension-pack",
                "twxs.cmake",
                "ms-vscode.cmake-tools",
                "ms-iot.vscode-ros",
                "smilerobotics.urdf",
                "yzhang.markdown-all-in-one"
            ]
        }
    }
}

# Prompt the user to choose shell configuration
shell_choice = input(
    "Which shell would you like to configure (bash/zsh)? ").strip().lower()

if shell_choice == 'zsh':
    zsh_mounts = [
        "source=${localEnv:HOME}/.zshrc,target=/root/.zshrc,type=bind,readonly",
        "source=${localEnv:HOME}/.zsh_history,target=/root/.zsh_history,type=bind"
    ]
    template["mounts"].extend(zsh_mounts)
    template["customizations"]["vscode"]["settings"].update({
        "terminal.integrated.defaultProfile.linux": "zsh",
        "terminal.integrated.profiles.linux": {
            "zsh": {
                "path": "/bin/zsh"
            }
        }
    })

elif shell_choice == 'bash':
    bash_mounts = [
        "source=${localEnv:HOME}/.bashrc,target=/root/.bashrc,type=bind,readonly",
        "source=${localEnv:HOME}/.bash_history,target=/root/.bash_history,type=bind"
    ]
    template["mounts"].extend(bash_mounts)
    template["customizations"]["vscode"]["settings"].update({
        "terminal.integrated.defaultProfile.linux": "bash",
        "terminal.integrated.profiles.linux": {
            "bash": {
                "path": "/bin/bash"
            }
        }
    })

# Ensure the directory exists
os.makedirs(".devcontainer", exist_ok=True)

# Write the JSON data to the file
devcontainer_json_path = ".devcontainer/devcontainer.json"
if confirm_overwrite(devcontainer_json_path):
    with open(devcontainer_json_path, "w") as f:
        json.dump(template, f, indent=2)
    print("devcontainer.json has been created successfully.")
else:
    print("Skipped creating devcontainer.json.")

# Dockerfile content
dockerfile_content = """# This is a development environment
FROM registry.gitlab.com/polymathrobotics/autonomy/polymath_autonomy/humble:latest

RUN sudo apt-get update && sudo apt-get install -y \\
    valgrind vim python3-rocker can-utils ros-$ROS_DISTRO-foxglove-bridge

# Force disable opengl
ENV LIBGL_ALWAYS_SOFTWARE=1
"""

if shell_choice == 'zsh':
    dockerfile_content += """
# Shell stuff. Change to bash if that's your flavor
RUN sudo apt-get update && sudo apt install zsh -y
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
RUN chsh -s /bin/zsh
RUN /bin/zsh
"""

# Write the Dockerfile
dockerfile_path = ".devcontainer/Dockerfile"
if confirm_overwrite(dockerfile_path):
    with open(dockerfile_path, "w") as f:
        f.write(dockerfile_content)
    print("Dockerfile has been created successfully.")
else:
    print("Skipped creating Dockerfile.")
