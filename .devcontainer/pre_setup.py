import os

# Ensure the /tmp/mcap directory exists
mcap_directory = "/tmp/mcap"
if not os.path.exists(mcap_directory):
    os.makedirs(mcap_directory)
    print(f"Directory {mcap_directory} created.")
else:
    print(f"Directory {mcap_directory} already exists.")
