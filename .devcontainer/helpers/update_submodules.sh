#!/bin/bash

# Pull latest changes for the main repository
git pull origin main

# Initialize and update all submodules
git submodule update --init --recursive

# Loop through each submodule and update if no local changes
git submodule foreach '
    has_local_changes() {
        git diff-index --quiet HEAD -- || return 0
        git diff-index --cached --quiet HEAD -- || return 0
        return 1
    }

    if has_local_changes; then
        echo "Skipping submodule $sm_path due to local changes"
    else
        echo "Updating submodule $sm_path"
        git submodule update --remote --merge
    fi
'

