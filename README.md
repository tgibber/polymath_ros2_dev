# polymath_workspace

If you haven't read on up on VSCode devcontainers already, [please do so](https://code.visualstudio.com/docs/devcontainers/containers). They give us the remarkable ability to tune development environments which has historically taken entire infra teams to maintain and support. 

Think of this repository like a monorepo. It has all the stuff you need to build and run robot stuff, with a lot of harnessing already built out for you.

**But most importantly, it gives us all at Polymath a central area where we can develop, build and run code consistently!** 

It takes some strong stances on certain aspects of your development environment (e.g some vscode extensions you need to use) but on the otherhand lets you choose you own flavors in other areas (e.g. bash or zsh). 


## Setup

_Running into problems? Post in #polymath_workspace_


### Prereqs

- Ubuntu >22.04
- VSCode version >1.89.0
- VSCode Dev Container plugin
- Docker
- Access to gitlab docker registry (`docker login registry.gitlab.com`)
  - Use your gitlab credentials

### 1. Clone

Since this repository relies on git submodules, yoAutomatic Sourcing
u need a special command to clone:

```
git clone --recurse-submodules -j8 [REPO_URL]
```

### 2. Init Your `devcontainer.json`

In order to allow custom configurations of your `devcontainer.json`, you need to manually generate it via a helper script:

```
python3 ./.devcontainer/init.py
```

This script will produce:
- `.devcontainer/Dockerfile`
- `.devcontainer/devcontainer.json`

Which are both necessary for building your devcontainer with all the appropriate settings.

### 3. SSH forwarding

In order for your integrated terminal in VSCode to be able to push/pull changes in Git, you'll need to share your ssh credentials from your host machine. 

Primarily refer to docs [here](https://code.visualstudio.com/remote/advancedcontainers/sharing-git-credentials).

Make sure you have the SSH Agent running:
```
eval "$(ssh-agent -s)"
```

And add the following to your `.bashrc`/`.zshrc`:

```
if [ -z "$SSH_AUTH_SOCK" ]; then
   # Check for a currently running instance of the agent
   RUNNING_AGENT="`ps -ax | grep 'ssh-agent -s' | grep -v grep | wc -l | tr -d '[:space:]'`"
   if [ "$RUNNING_AGENT" = "0" ]; then
        # Launch a new instance of the agent
        ssh-agent -s &> $HOME/.ssh/ssh-agent
   fi
   eval `cat $HOME/.ssh/ssh-agent`
fi
```

### 4. Setup automatic sourcing

Never run `source /opt/polymathrobotics/setup.zsh` again!

In either your `.bashrc` or `.zshrc`, put:

```
if [[ "$ROS_DISTRO" == "humble" ]]; then
  source ./install/setup.zsh
  source /opt/polymathrobotics/setup.zsh
  source /opt/ros/humble/setup.zsh
fi
```

### 5. Build container

Now hopefully everything is good to try your first build!

Open up command pallette (`ctrl + shift + p`) and select `Dev Containers: Reopen in Container`.

This will open up a new VSCode window and will start building your container.

If you run into errors, see the "Troubleshooting" section below.


## Structure

This repository roughly follows the structure of our Gitlab repository structure, with all the sub repos under `/src`. 

We rely on git submodules here, which has its own set of unique hurdles. Please read "Dealing With Git Submodules" below.

Generally run your `colcon` workflow from the root of the directory. Our devcontainer mounts volumes into your host environment such that between container rebuilds, old `build`, `install` and `log` directories at the top level are persisted. You'll also notice that we've put in our standard `dds_config.xml` at the top level as well.

## Usage

### C++ Intellisense

If you want to get proper intellisense working with C++, you'll want to run `colcon build` with `--cmake-args "-DCMAKE_EXPORT_COMPILE_COMMANDS=On"`. E.g:

```
colcon build --cmake-args "-DCMAKE_EXPORT_COMPILE_COMMANDS=On" --packages-select radar_layer 
```

This will place `compile_commands.json` into your top level build folder, which VSCode uses to map your C++ workspace.


### Python

Most python tooling should work out of the box.


### Dealing with git submodules

Using Git Submodules is very different than pure git. 

#### Pulling all latest changes

1. Goto command pallette: `ctrl + shift + p`
2. Select `Run Task`
3. Select `Update Git Submodules`

#### Pulling in changes for just one git submodule

1. `cd` into submodule
2. `git pull origin main`

### Adding a new Git Submodule

1. `cd` into directory you'd like to add the new git submodule
2. Run `git submodule add [repo_url]
3. Open up a merge request


## Contributing

Contributions are welcome! To make this the best it can be we'll need a lot of diverse ideas. 

### Guidelines

- Open up merge requests into `main` and post in `#code-reviews`
- As a rule of thumb, only include ROS repos. For instance, none of the infrastructure repos really belong here
- If you have a specific vscode setting you'd like to add, upate the `init.py` script. Think about whether it applies just to you or should apply to everyone.


## Troubleshooting

If all else fails, post your problems in #polymath_workspace.

### General Sanity Checks

- `Developer: Reload Window`: Access from command pallette. Generally a good idea if you're seeing any container issues
- `Clean colcon workspace`: Available as a task via command pallette. Removes build, install and log directories.

### Process Still Alive After Reloading Container

Sometimes your nodes will live on after reloading a container, making it impossible to kill the process via terminal. Here are some steps to clean things up.

One liner for killing everything:
```
ps aux | grep /ros/humble | grep -v grep | awk '{print $2}' | sudo xargs kill -9
```

Run `ros2 node list` to see if everything has indeed come down.


### GUIs don't work e.g. rviz, gazebo

You likely need to grant docker access to your X server and connect to graphical displays. To fix this issue, run:

```
xhost +local:
```

